import unittest
from pad_655 import buy_or_sell


class TestPad655(unittest.TestCase):

    def test_buy_or_sell(self):
        self.assertEqual(
            buy_or_sell([["apple", "orange"], ["orange", "pear"], ["apple", "pear"]], "apple"),
            ["buy", "buy", "sell"]
        )
