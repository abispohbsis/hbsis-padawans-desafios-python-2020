import unittest
from pad_660 import is_valid_walk


class TestPad660(unittest.TestCase):

    def test_is_valid_walk(self):
        self.assertTrue(is_valid_walk(['n', 's', 'n', 's', 'n', 's', 'n', 's', 'n', 's']))
        self.assertFalse(is_valid_walk(['w']))
