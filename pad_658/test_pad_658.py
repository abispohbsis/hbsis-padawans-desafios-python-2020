import unittest
from pad_658 import likes


class TestPad658(unittest.TestCase):

    def test_likes(self):
        self.assertEqual(likes(['Alex', 'Jacob', 'Mark', 'Max']), 'Alex, Jacob and 2 others like this')
