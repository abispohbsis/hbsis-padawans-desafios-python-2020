import unittest
from pad_648 import vowel_count


class TestPad648(unittest.TestCase):

    def test_vowel_count(self):
        self.assertEqual(vowel_count("abracadabra"), 5)
