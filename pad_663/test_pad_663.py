import unittest
from pad_663 import tickets


class TestPad663(unittest.TestCase):

    def test_tickets(self):
        self.assertEqual(tickets([25, 25, 50]), "YES")
        self.assertEqual(tickets([25, 50]), "NO")
