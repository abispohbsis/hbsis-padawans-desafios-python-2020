import unittest
from pad_661 import order


class TestPad661(unittest.TestCase):

    def test_order(self):
        self.assertEqual(order("4of Fo1r pe6ople g3ood th5e the2"), "Fo1r the2 g3ood 4of th5e pe6ople")
