import unittest
from pad_659 import duplicate_count


class TestPad659(unittest.TestCase):

    def test_duplicate_count(self):
        self.assertEqual(duplicate_count("abcdea"), 1)
