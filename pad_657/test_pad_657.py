import unittest
from pad_657 import multiple_of_3_or_5


class TestPad657(unittest.TestCase):

    def test_multiple_of_3_or_5(self):
        self.assertEqual(multiple_of_3_or_5(10), 23)
