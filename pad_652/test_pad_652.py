import unittest
from pad_652 import db_sort


class TestPad652(unittest.TestCase):

    def test_db_sort(self):
        self.assertEqual(db_sort(
            ["Banana", "Orange", "Apple", "Mango", 0, 2, 2]),
            [0, 2, 2, "Apple", "Banana", "Mango", "Orange"]
        )
