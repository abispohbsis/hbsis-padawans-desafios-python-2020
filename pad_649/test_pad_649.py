import unittest
from pad_649 import find_short


class TestPad649(unittest.TestCase):

    def test_find_short(self):
        self.assertEqual(find_short("bitcoin take over the world maybe who knows perhaps"), 3)
