import unittest
from pad_664 import count_smileys


class TestPad664(unittest.TestCase):

    def test_count_smileys(self):
        self.assertEqual(count_smileys([':D', ':~)', ';~D', ':)']), 4)
