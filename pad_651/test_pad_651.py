import unittest
from pad_651 import square_digits


class TestPad651(unittest.TestCase):

    def test_square_digits(self):
        self.assertEqual(square_digits(square_digits(9119), 811181))
