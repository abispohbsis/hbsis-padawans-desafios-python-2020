import unittest
from pad_656 import missing_sum


class TestPad656(unittest.TestCase):

    def test_missing_sum(self):
        self.assertEqual(missing_sum([1, 2, 8, 7]), 4)
