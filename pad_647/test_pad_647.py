import unittest
from pad_647 import accum


class TestPad647(unittest.TestCase):

    def test_accum(self):
        self.assertEqual(
            accum("ZpglnRxqenU"),
            "Z-Pp-Ggg-Llll-Nnnnn-Rrrrrr-Xxxxxxx-Qqqqqqqq-Eeeeeeeee-Nnnnnnnnnn-Uuuuuuuuuuu"
        )
