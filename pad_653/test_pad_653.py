import unittest
from pad_653 import growing_plant


class TestPad653(unittest.TestCase):

    def test_growing_plant(self):
        self.assertEqual(growing_plant(100, 10, 910), 10)
